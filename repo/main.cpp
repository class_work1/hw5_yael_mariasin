#include <string>
#include <vector>
#include <algorithm>
#include <iterator>
#include <iostream>
#include <fstream> 
#include <sstream>
#include <stdio.h>
#include <Windows.h>

using namespace std;
#include "Helper.h"
#define PATH_MAX 2048

string pwdFun();

typedef int (CALLBACK* TheAnswerToLifeTheUniverseAndEverything)();

int main()
{
	Helper h;
	string message;
	vector<string> v;

	while (1)
	{
		cout << ">>";
		//cin >> message;
		std::getline(std::cin, message);
		v = h.get_words(message);
		
		if (v[0] == "pwd")
		{
			cout << pwdFun() << endl;
		}
		else if (v[0] == "cd" && v.size() >= 2)
		{
			string path = pwdFun() + "\\" + v[1];
			if (!SetCurrentDirectory(path.c_str()))
			{//error
				cout << "SetCurrentDirectory failed " << GetLastError() << endl;
				return 1;
			}
		}
		else if (v[0] == "create" && v.size() >= 2)
		{
			ofstream myfile(v[1]);
			if (myfile.is_open())
			{
				myfile.close();

				string path = pwdFun() + "\\" + v[1];
				//const char* p_ch = path.c_str();

				if (remove(path.c_str()) != 0)
				{
					cout << "Couldn't replace the file";
					return 1;
				}
				//else File successfully deleted
			}
			//else there is no file with this name
			try
			{
				std::ofstream outfile(v[1]);
			}
			catch (exception e)
			{
				cout << "could create the file" << endl;
			}
			
		}
		else if (v[0] == "ls")
		{
			HANDLE hFind;
			WIN32_FIND_DATA data;
			string path = pwdFun() + "\\*.*";
			
			hFind = FindFirstFile(path.c_str(), &data);
			if (hFind != INVALID_HANDLE_VALUE) {
				do {
					if (data.cFileName[0] != '.')
					{
						printf("%s\n", data.cFileName);
					}
				} while (FindNextFile(hFind, &data));
				FindClose(hFind);
			}
		}
		else if (v[0] == "secret")
		{
			BOOL freeResult, runTimeLinkSuccess = FALSE;
			HINSTANCE dllHandle = NULL;

			dllHandle = LoadLibrary("secret.dll");

			if (NULL != dllHandle)
			{
				TheAnswerToLifeTheUniverseAndEverything TheAnswerToLifeTheUniverseAndEverythingPtr = NULL;
				//Get pointer to our function using GetProcAddress:
				TheAnswerToLifeTheUniverseAndEverythingPtr = (TheAnswerToLifeTheUniverseAndEverything)GetProcAddress(dllHandle,
					"FindArtist");
				if (NULL != TheAnswerToLifeTheUniverseAndEverythingPtr)
				{
					int ans;
					ans = TheAnswerToLifeTheUniverseAndEverythingPtr();
					cout << ans << endl;
				}

				
			}
			if (!runTimeLinkSuccess)
				printf("message via alternative method\n");
		}
	}


	return 0;
}

string pwdFun()
{
	TCHAR pwd[MAX_PATH];
	GetCurrentDirectory(MAX_PATH, pwd);
	return pwd;
}
